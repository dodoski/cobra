import Calculator.*;


import org.omg.CORBA.*;
import org.omg.CosNaming.*;
import org.omg.CosNaming.NamingContextPackage.*;
import org.omg.PortableServer.*;
import org.omg.PortableServer.POA;
import java.util.Properties;

class CalculatorInterfaceImpl extends CalculatorInterfacePOA
{
	private ORB orb;
	String result;
	public void setORB(ORB orb_val){
		orb = orb_val;
	}
	
	public String add(double a, double b, double c, double d){
		double re = a + c;
		double im = b + d;
		result = "Re: " + re + " Im: " + im;
		return result;
	}
	public String subtract(double a, double b, double c, double d){
		double re = a - c;
		double im = b - d;
		result = "Re: " + re + " Im: " + im;
		return result;
	}
	public String multiply(double a, double b, double c, double d){
		double re = (a * c) - (b * d);
		double im = (a * d) + (b * c);
		result = "Re: " + re + " Im: " + im;
		return result;
	}
	public String divide(double a, double b, double c, double d){
		double re = ((a*c) + (b*d))/((c*c) + (d*d));
		double im = ((b*c) - (a*d))/((c*c)+(d*d));
		result = "Re: " + re + " Im: " + im;
		return result;
	}
	public void shutdown(){
		orb.shutdown(false);
	}
}

public class CalculatorServer{
	public static void main(String args[]){
		try{
		ORB orb = ORB.init(args, null);
		POA rootpoa = POAHelper.narrow(orb.resolve_initial_references("RootPOA"));
		rootpoa.the_POAManager().activate();
		CalculatorInterfaceImpl CalculatorImpl = new CalculatorInterfaceImpl();
		CalculatorImpl.setORB(orb);
		org.omg.CORBA.Object ref=rootpoa.servant_to_reference(CalculatorImpl);
		CalculatorInterface href=CalculatorInterfaceHelper.narrow(ref);
		org.omg.CORBA.Object objRef = orb.resolve_initial_references("NameService");
		NamingContextExt ncRef = NamingContextExtHelper.narrow(objRef);//związanie obiektu z nazwą
		String name= "KalkulatorOperations"; 
		NameComponent path[] = ncRef.to_name(name);
		ncRef.rebind(path,href);
		System.out.println("\n----------------------------------------------");
		System.out.println("Kalkulator: Gotowy i czeka...");
		System.out.println("\n----------------------------------------------");
		
		orb.run();
		}
			catch(Exception e){
				System.out.println("\n----------------------------------------------");
				System.out.println("Kalkulator: BLAD !");
				System.out.println("\n----------------------------------------------");
				}
			System.out.println("\nKalkulator: Wyjscie...");
			System.out.println("\n----------------------------------------------");
	}
}