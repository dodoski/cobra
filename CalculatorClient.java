import Calculator.*;
import org.omg.CosNaming.*;
import org.omg.CosNaming.NamingContextPackage.*;
import org.omg.CORBA.*;
import java.util.Scanner;
import java.lang.*;

public class CalculatorClient{
	static CalculatorInterface CalculatorImpl;
	public static void main(String args[]){
		
		try{
			ORB orb = ORB.init(args, null);
			org.omg.CORBA.Object objRef=orb.resolve_initial_references("NameService");;
			NamingContextExt ncRef = NamingContextExtHelper.narrow(objRef);
			String name = "KalkulatorOperations";
			CalculatorImpl = CalculatorInterfaceHelper.narrow(ncRef.resolve_str(name));
			System.out.println("\nKalkulatorClient: Dodawanie\t= "+ CalculatorImpl.add(5,2,3,-9));
			System.out.println("\nKalkulatorClient: Odejmowanie\t= "+ CalculatorImpl.subtract(-3,1,2,-5));
			System.out.println("\nKalkulatorClient: Mnożenie\t= "+ CalculatorImpl.multiply(4,1,2,-5));
			System.out.println("\nKalkulatorClient: Dzielenie\t= "+ CalculatorImpl.subtract(1,8,2,3));
			CalculatorImpl.shutdown();
		} catch (Exception e){
			System.out.println(e.getMessage());
		}
	}
}